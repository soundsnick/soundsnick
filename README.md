### Hi, there, it's Yernazar!

- [note] If you want to check out my old contributions, check out [github](https://github.com/soundsnick)
- [for hiring purposes] [CV](https://github.com/soundsnick/soundsnick/raw/main/cv.pdf) 
- [contact] You can find me on Telegram [@yarnduffold](https://t.me/yarnduffold)
